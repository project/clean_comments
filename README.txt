Module description
------------------
Clean comments is a plug & play module that cleans up the display of
comments for usability and readabilty of the comment threads.


How it works
------------------
Clean comments adds a small js file (clean_comments.js) to every page
load.

For now it only hides the comment links (delete, edit, reply, etc), and
when the user hovers over the comment, it toggles the links visibility.

The height of the comment block element is set through JS inline at the
begining in order to avoid jumping when toggling the links visiblity.

Note that the js file could be added only if there are comments in the
page, but it would mean making your users have download different
bundles of js files, and in the end hurting frontend performance.


How to use it
------------------
Simply install the module as usual
http://drupal.org/documentation/install/modules-themes/modules-7

For now there are no settings to configure.

The selector for the comments is hardcoded to '#comments div.comment',
which is what Drupal renders by default. I normaly never see this
rendered any other way, so for now we don't include a setting to
configure this. Feel free to open a feature request if necessary.