(function ($) {
  Drupal.behaviors.cleanComments = {
    attach: function (context, settings) {
      var $comments = $('#comments div.comment', context).each(function() {
        // Set the original height as fixed to avoid jumping when
        // we hide the links later.
        var $this = $(this);
        var height = $this.height();
        $this.height(height);
      });

      // Hide all comment links to start with.
      $('ul.links', $comments).hide();

      // Toggle links visibility when hovering the comment.
      $comments.hover(
        function () {
          $(this).find('ul.links').toggle();
        },
        function () {
          $(this).find('ul.links').toggle();
        }
      );
    }
  };
})(jQuery);